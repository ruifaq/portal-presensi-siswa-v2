import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async info(){
    this.router.navigate(['/report-info'])
  }

}
