import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PresensiFormPage } from './presensi-form.page';

describe('PresensiFormPage', () => {
  let component: PresensiFormPage;
  let fixture: ComponentFixture<PresensiFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresensiFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PresensiFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
