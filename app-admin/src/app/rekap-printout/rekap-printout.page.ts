import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rekap-printout',
  templateUrl: './rekap-printout.page.html',
  styleUrls: ['./rekap-printout.page.scss'],
})
export class RekapPrintoutPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/rekap-print'])
  }

}

