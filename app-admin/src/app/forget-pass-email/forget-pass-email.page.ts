import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-forget-pass-email',
  templateUrl: './forget-pass-email.page.html',
  styleUrls: ['./forget-pass-email.page.scss'],
})
export class ForgetPassEmailPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async forgetfinal(){
    this.router.navigate(['/forget-pass-final'])
  }

}
