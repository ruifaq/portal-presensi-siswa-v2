import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RekapPrintPage } from './rekap-print.page';

const routes: Routes = [
  {
    path: '',
    component: RekapPrintPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RekapPrintPageRoutingModule {}
