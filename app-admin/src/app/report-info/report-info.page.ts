import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'


@Component({
  selector: 'app-report-info',
  templateUrl: './report-info.page.html',
  styleUrls: ['./report-info.page.scss'],
})
export class ReportInfoPage implements OnInit {

  constructor(
    public router :Router
  ) { }

  ngOnInit() {
  }

  async info(){
    this.router.navigate(['/'])
  }

  async back(){
    this.router.navigate(['/report'])
  }

  async ok(){
    this.router.navigate(['/report'])
  }
}
