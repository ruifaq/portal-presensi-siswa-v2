import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'


@Component({
  selector: 'app-jadwal',
  templateUrl: './jadwal.page.html',
  styleUrls: ['./jadwal.page.scss'],
})
export class JadwalPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async tambahData(){
    this.router.navigate(['/tambah-data-mapel'])
  }

}
