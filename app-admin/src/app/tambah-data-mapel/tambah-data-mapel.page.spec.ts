import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahDataMapelPage } from './tambah-data-mapel.page';

describe('TambahDataMapelPage', () => {
  let component: TambahDataMapelPage;
  let fixture: ComponentFixture<TambahDataMapelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahDataMapelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahDataMapelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
