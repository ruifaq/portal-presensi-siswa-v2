import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-tambah-data-mapel',
  templateUrl: './tambah-data-mapel.page.html',
  styleUrls: ['./tambah-data-mapel.page.scss'],
})
export class TambahDataMapelPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/jadwal'])
  }

  async tambahData(){
    this.router.navigate(['/jadwal'])
  }
}
