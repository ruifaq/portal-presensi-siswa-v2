import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahDataMapelPage } from './tambah-data-mapel.page';

const routes: Routes = [
  {
    path: '',
    component: TambahDataMapelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahDataMapelPageRoutingModule {}
