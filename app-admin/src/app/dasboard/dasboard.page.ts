import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.page.html',
  styleUrls: ['./dasboard.page.scss'],
})
export class DasboardPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async jadwal(){
    this.router.navigate(['/jadwal'])
  }

  async rekap(){
    this.router.navigate(['/rekap'])
  }

  async report(){
    this.router.navigate(['/report'])
  }

  async presensi(){
    this.router.navigate(['/presensi-form'])
  }

  async profile(){
    this.router.navigate(['/profile'])
  }

}
