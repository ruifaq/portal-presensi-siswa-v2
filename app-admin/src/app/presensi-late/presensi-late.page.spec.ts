import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PresensiLatePage } from './presensi-late.page';

describe('PresensiLatePage', () => {
  let component: PresensiLatePage;
  let fixture: ComponentFixture<PresensiLatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresensiLatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PresensiLatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
