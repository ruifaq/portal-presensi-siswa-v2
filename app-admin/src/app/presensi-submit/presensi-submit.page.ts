import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-presensi-submit',
  templateUrl: './presensi-submit.page.html',
  styleUrls: ['./presensi-submit.page.scss'],
})
export class PresensiSubmitPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/presensi-form'])
  }

  async next(){
    this.router.navigate(['/dasboard'])
  }

}
