import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, ModalController } from '@ionic/angular';
import { ProfilePage } from '../profile/profile.page';


@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.page.html',
  styleUrls: ['./dasboard.page.scss'],
})
export class DasboardPage implements OnInit {

  constructor(
    public router: Router,
    private menu: MenuController,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async jadwal(){
    this.router.navigate(['/jadwal'])
  }

  async rekap(){
    this.router.navigate(['/rekap'])
  }

  async report(){
    this.router.navigate(['/report'])
  }

  async presensi(){
    this.router.navigate(['/presensi-form'])
  }

  async presensiOffline(){
    this.router.navigate(['/presensi-offline'])
  }

  async profile() {
    const modal = await this.modalController.create({
      component: ProfilePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }


}
