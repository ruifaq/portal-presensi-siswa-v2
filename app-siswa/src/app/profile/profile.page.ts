import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router'



@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
    public router: Router
  ) { }

  ngOnInit() {
  }
  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  async logout(){
    this.router.navigate(['/login'])
    this.modalCtrl.dismiss({
      'dismissed' : true
    });
  }
}
