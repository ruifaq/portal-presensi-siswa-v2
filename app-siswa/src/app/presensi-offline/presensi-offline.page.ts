import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-presensi-offline',
  templateUrl: './presensi-offline.page.html',
  styleUrls: ['./presensi-offline.page.scss'],
})
export class PresensiOfflinePage implements OnInit {

  constructor(
    public router :Router
  ) { }

  ngOnInit() {
  }

  async scan(){
    this.router.navigate(['/presensi-qrcode'])
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

}
