import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { report } from 'process';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async report(){
    this.router.navigate(['/dasboard'])
  }

}
