import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'


@Component({
  selector: 'app-jadwal',
  templateUrl: './jadwal.page.html',
  styleUrls: ['./jadwal.page.scss'],
})
export class JadwalPage implements OnInit {

  constructor(
    public router: Router,
    public http: HttpClient
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  loadData(){
    this.http.get('')
  }

}
