import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-rekap-print',
  templateUrl: './rekap-print.page.html',
  styleUrls: ['./rekap-print.page.scss'],
})
export class RekapPrintPage implements OnInit {

  constructor(
    public router:Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/rekap'])
  }

}
