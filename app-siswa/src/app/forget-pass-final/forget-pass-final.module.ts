import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgetPassFinalPageRoutingModule } from './forget-pass-final-routing.module';

import { ForgetPassFinalPage } from './forget-pass-final.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgetPassFinalPageRoutingModule
  ],
  declarations: [ForgetPassFinalPage]
})
export class ForgetPassFinalPageModule {}
