import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresensiQrcodePage } from './presensi-qrcode.page';

const routes: Routes = [
  {
    path: '',
    component: PresensiQrcodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PresensiQrcodePageRoutingModule {}
