import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PresensiQrcodePageRoutingModule } from './presensi-qrcode-routing.module';

import { PresensiQrcodePage } from './presensi-qrcode.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PresensiQrcodePageRoutingModule
  ],
  declarations: [PresensiQrcodePage]
})
export class PresensiQrcodePageModule {}
