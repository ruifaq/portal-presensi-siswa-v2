import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-presensi-form',
  templateUrl: './presensi-form.page.html',
  styleUrls: ['./presensi-form.page.scss'],
})
export class PresensiFormPage implements OnInit {

  constructor(
    public router:Router
  ) { }

  ngOnInit() {
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async kirim(){
    this.router.navigate(['/presensi-submit'])
  }


}
