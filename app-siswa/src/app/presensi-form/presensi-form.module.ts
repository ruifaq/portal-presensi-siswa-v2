import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PresensiFormPageRoutingModule } from './presensi-form-routing.module';

import { PresensiFormPage } from './presensi-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PresensiFormPageRoutingModule
  ],
  declarations: [PresensiFormPage]
})
export class PresensiFormPageModule {}
