import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dasboard',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'regis',
    loadChildren: () => import('./regis/regis.module').then( m => m.RegisPageModule)
  },
  {
    path: 'dasboard',
    loadChildren: () => import('./dasboard/dasboard.module').then( m => m.DasboardPageModule)
  },
  {
    path: 'jadwal',
    loadChildren: () => import('./jadwal/jadwal.module').then( m => m.JadwalPageModule)
  },
  {
    path: 'rekap',
    loadChildren: () => import('./rekap/rekap.module').then( m => m.RekapPageModule)
  },
  {
    path: 'report',
    loadChildren: () => import('./report/report.module').then( m => m.ReportPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'forget-pass',
    loadChildren: () => import('./forget-pass/forget-pass.module').then( m => m.ForgetPassPageModule)
  },
  {
    path: 'forget-pass-email',
    loadChildren: () => import('./forget-pass-email/forget-pass-email.module').then( m => m.ForgetPassEmailPageModule)
  },
  {
    path: 'forget-pass-phone',
    loadChildren: () => import('./forget-pass-phone/forget-pass-phone.module').then( m => m.ForgetPassPhonePageModule)
  },
  {
    path: 'forget-pass-final',
    loadChildren: () => import('./forget-pass-final/forget-pass-final.module').then( m => m.ForgetPassFinalPageModule)
  },
  {
    path: 'presensi-form',
    loadChildren: () => import('./presensi-form/presensi-form.module').then( m => m.PresensiFormPageModule)
  },
  {
    path: 'presensi-submit',
    loadChildren: () => import('./presensi-submit/presensi-submit.module').then( m => m.PresensiSubmitPageModule)
  },
  {
    path: 'presensi-late',
    loadChildren: () => import('./presensi-late/presensi-late.module').then( m => m.PresensiLatePageModule)
  },
  {
    path: 'rekap-print',
    loadChildren: () => import('./rekap-print/rekap-print.module').then( m => m.RekapPrintPageModule)
  },
  {
    path: 'presensi-offline',
    loadChildren: () => import('./presensi-offline/presensi-offline.module').then( m => m.PresensiOfflinePageModule)
  },
  {
    path: 'presensi-qrcode',
    loadChildren: () => import('./presensi-qrcode/presensi-qrcode.module').then( m => m.PresensiQrcodePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
