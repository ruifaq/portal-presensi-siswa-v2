import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
import firebase from 'firebase/app'
import { AngularFireDatabaseModule } from '@angular/fire/database';


@Component({
  selector: 'app-regis',
  templateUrl: './regis.page.html',
  styleUrls: ['./regis.page.scss'],
})
export class RegisPage implements OnInit {

  username: string = ""
  password: string = ""
  cpassword: string = ""

  constructor(
    public auth: AngularFireAuth,   
    private afDB: AngularFireDatabaseModule
    ) { }

  ngOnInit() {
  }

  async regis() {
    const { username, password, cpassword } = this
    if(password !== cpassword) {
      return console.log("Password salah!?")
      
    }
    try {
      const res = await this.auth.createUserWithEmailAndPassword(username + ("@firebase.com"), password)
      console.log(res)
    }catch(error) {
      console.dir(error)
    }
  
  }

}
