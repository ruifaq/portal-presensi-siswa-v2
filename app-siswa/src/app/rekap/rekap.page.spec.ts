import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RekapPage } from './rekap.page';

describe('RekapPage', () => {
  let component: RekapPage;
  let fixture: ComponentFixture<RekapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RekapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
